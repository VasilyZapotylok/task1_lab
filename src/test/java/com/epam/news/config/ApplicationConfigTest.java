package com.epam.news.config;

import java.util.Locale;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.dao.CommentDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.dao.impl.AuthorDAOImpl;
import com.epam.news.dao.impl.CommentDAOImpl;
import com.epam.news.dao.impl.NewsDAOImpl;
import com.epam.news.dao.impl.TagDAOImpl;

/**
 * The Class ApplicationConfigTest.
 */
@Configuration
@PropertySource("classpath:db_for_test.properties")
public class ApplicationConfigTest {

	/** The environment. */
	@Autowired
	private Environment environment;

	/**
	 * Data source.
	 *
	 * @return the data source
	 */
	@Bean (name = "dataSourceTest")
	public DataSource dataSource() {
		Locale.setDefault(new Locale("en", "EN"));
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(environment.getRequiredProperty("db.driver"));
		dataSource.setUrl(environment.getRequiredProperty("db.url"));
		dataSource.setUsername(environment.getRequiredProperty("db.username"));
		dataSource.setPassword(environment.getRequiredProperty("db.password"));
		dataSource.setInitialSize(Integer.parseInt(environment.getRequiredProperty("db.initialsize")));
		return dataSource;
	}

	/**
	 * Creates the tag dao.
	 *
	 * @return the tag dao
	 */
	@Bean (name = "TagDAOTest")
	public TagDAO createTagDAO() {
		return new TagDAOImpl();
	}

	/**
	 * Creates the comment dao.
	 *
	 * @return the comment dao
	 */
	@Bean (name = "CommentDAOTest")
	public CommentDAO createCommentDAO() {
		return new CommentDAOImpl();
	}

	/**
	 * Creates the news dao.
	 *
	 * @return the news dao
	 */
	@Bean (name = "NewsDAOTest")
	public NewsDAO createNewsDAO() {
		return new NewsDAOImpl();
	}

	/**
	 * Creates the author dao.
	 *
	 * @return the author dao
	 */
	@Bean (name = "AuthorDAOTest")
	public AuthorDAO createAuthorDAO() {
		return new AuthorDAOImpl();
	}

}
