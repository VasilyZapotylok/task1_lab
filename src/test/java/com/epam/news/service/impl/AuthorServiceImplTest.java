package com.epam.news.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.news.dao.impl.AuthorDAOImpl;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;


/**
 * The Class AuthorServiceImplTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest {

	/** The author dao mock. */
	@Mock
	private AuthorDAOImpl authorDaoMock;

	/** The author service. */
	@InjectMocks
	private AuthorServiceImpl authorService;

	/**
	 * Adds the author.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void addAuthor() throws ServiceException, DAOException {
		when(authorDaoMock.add(any(Author.class))).thenAnswer(new Answer<Long>() {
			public Long answer(InvocationOnMock invocation) throws Throwable {
				return 1L;
			}
		});
		Author author = new Author();
		author.setAuthorId(1L);
		author.setAuthorName("authorName");
		long authorId = authorService.add(author);
		assertEquals(1l, authorId);
	}

	/**
	 * Update author.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void updateAuthor() throws ServiceException, DAOException {
		Author author = new Author();
		author.setAuthorId(1L);
		author.setAuthorName("authorName");
		authorService.update(author);
		verify(authorDaoMock).update(author);
	}

	/**
	 * Del author.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void delAuthor() throws ServiceException, DAOException {
		authorService.deleteById(1L);
		verify(authorDaoMock).deleteById(1L);
	}

	/**
	 * Fidn author by id.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void fidnAuthorById() throws ServiceException, DAOException {
		when(authorDaoMock.findById(any(Long.class))).thenAnswer(new Answer<Author>() {
			public Author answer(InvocationOnMock invocation) throws Throwable {
				Author author = new Author();
				author.setAuthorId(((Long) invocation.getArguments()[0]));
				author.setAuthorName("authorName");
				return author;
			}
		});
		Author author = new Author();
		author.setAuthorId(1L);
		author.setAuthorName("authorName");
		Author authorFound = authorService.findById(1L);
		assertEquals(author, authorFound);
	}

	/**
	 * Fidn author by news id.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void fidnAuthorByNewsId() throws ServiceException, DAOException {
		when(authorDaoMock.findByNewsId(any(Long.class))).thenAnswer(new Answer<List<Author>>() {
			public List<Author> answer(InvocationOnMock invocation) throws Throwable {
				List<Author> list = new ArrayList<Author>();
				list.add(new Author());
				list.add(new Author());
				list.add(new Author());
				return list;
			}
		});
		List<Author> listFound  = authorService.findByNewsId(1L);
		assertEquals(3, listFound.size());
	}


	/**
	 * Expire author.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void expireAuthor() throws ServiceException, DAOException {
		Author author = new Author();
		author.setAuthorId(1L);
		author.setAuthorName("authorName");
		authorService.expireAuthor(author);
		verify(authorDaoMock).expireAuthor(author);
	}

}
