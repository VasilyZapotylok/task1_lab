package com.epam.news.service.impl;


import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.news.dao.impl.TagDAOImpl;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;

/**
 * The Class TagServiceImplTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {

	/** The tag dao mock. */
	@Mock
	private TagDAOImpl tagDaoMock;

	/** The tag service. */
	@InjectMocks
	private TagServiceImpl tagService;

	/**
	 * Adds the tag.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void addTag() throws ServiceException, DAOException {
		Tag tag = new Tag();
		tag.setTagName("tagName");
		tagService.add(tag);
		verify(tagDaoMock).add(tag);
	}

	/**
	 * Adds the tag detail.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void addTagDetail() throws ServiceException, DAOException {
		when(tagDaoMock.add(any(Tag.class))).thenAnswer(new Answer<Long>() {
			public Long answer(InvocationOnMock invocation) throws Throwable {
				return 1L;
			}
		});
		Tag tag = new Tag();
		tag.setTagId(1L);
		tag.setTagName("tag");
		long tagId = tagService.add(tag);
		assertEquals(1l, tagId);
	}

	/**
	 * Find by id.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void findById() throws ServiceException, DAOException {
		when(tagDaoMock.findById(any(Long.class))).thenAnswer(new Answer<Tag>() {
			public Tag answer(InvocationOnMock invocation) throws Throwable {
				Tag tag = new Tag();
				tag.setTagId(((Long) invocation.getArguments()[0]));
				tag.setTagName("tag");
				return tag;
			}
		});
		Tag tag = new Tag();
		tag.setTagId(1L);
		tag.setTagName("tag");
		assertEquals(tag, tagService.findById(1L));
	}

	/**
	 * Update tag.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void updateTag() throws ServiceException, DAOException {
		Tag tag = new Tag();
		tag.setTagName("tagName");
		tagService.update(tag);
		verify(tagDaoMock).update(tag);
	}

	/**
	 * Delete by id.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void deleteById() throws ServiceException, DAOException {
		tagService.deleteById(1L);
		verify(tagDaoMock).deleteById(1L);
	}

	/**
	 * Find all tags.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 */
	@Test
	public void findAllTags() throws ServiceException, DAOException {
		when(tagDaoMock.findAll()).thenAnswer(new Answer<List<Tag>>() {
			@Override
			public List<Tag> answer(InvocationOnMock invocation) throws Throwable {
				List<Tag> tags = new ArrayList<>();
				tags.add(new Tag());
				tags.add(new Tag());
				tags.add(new Tag());
				return tags;
			}
		});
		List<Tag> tags = tagService.takeAll();
		assertEquals(3, tags.size());

	}


	/**
	 * Find tags by new id.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 */
	@Test
	public void findTagsByNewId() throws ServiceException, DAOException {
		when(tagDaoMock.findByNewsId(any(Long.class))).thenAnswer(new Answer<List<Tag>>() {
			@Override
			public List<Tag> answer(InvocationOnMock invocation) throws Throwable {
				List<Tag> tags = new ArrayList<>();
				tags.add(new Tag());
				tags.add(new Tag());
				tags.add(new Tag());
				return tags;
			}
		});
		List<Tag> tags = tagService.findByNewsId(1L);
		assertEquals(3, tags.size());

	}
}
