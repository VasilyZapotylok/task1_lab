package com.epam.news.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.news.dao.impl.CommentDAOImpl;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;

/**
 * The Class CommentServiceImplTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {

	/** The comment dao mock. */
	@Mock
	private CommentDAOImpl commentDaoMock;

	/** The comment service. */
	@InjectMocks
	private CommentServiceImpl commentService;

	/**
	 * Adds the comment.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void addComment() throws ServiceException, DAOException {
		when(commentDaoMock.add(any(Comment.class))).thenAnswer(new Answer<Long>() {
			public Long answer(InvocationOnMock invocation) throws Throwable {
				return 1L;
			}
		});
        Comment comment = new Comment();
        comment.setCommentId(1L);
        comment.setNewsId(1L);
        comment.setCommentText("commentText");
		long commentId = commentService.add(comment);
		assertEquals(1l, commentId);
	}

	/**
	 * Update comment.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void updateComment() throws ServiceException, DAOException {
		Comment comment = new Comment();
        comment.setCommentId(1L);
        comment.setNewsId(1L);
        comment.setCommentText("commentTextMod");
        commentService.update(comment);
		verify(commentDaoMock).update(comment);
	}

	/**
	 * Del author.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void delAuthor() throws ServiceException, DAOException {
		commentService.deleteById(1L);
		verify(commentDaoMock).deleteById(1L);
	}

	/**
	 * Fidn comment by id.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void fidnCommentById() throws ServiceException, DAOException {
		when(commentDaoMock.findById(any(Long.class))).thenAnswer(new Answer<Comment>() {
			public Comment answer(InvocationOnMock invocation) throws Throwable {
				Comment comment = new Comment();
		        comment.setCommentId(((Long) invocation.getArguments()[0]));
		        comment.setNewsId(1L);
		        comment.setCommentText("commentTextMod");
				return comment;
			}
		});
		Comment comment = new Comment();
        comment.setCommentId(1L);
        comment.setNewsId(1L);
        comment.setCommentText("commentTextMod");
		Comment commentFound = commentService.findById(1L);
		assertEquals(comment, commentFound);
	}

	/**
	 * Fidn all comments.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void fidnAllComments() throws ServiceException, DAOException {
		when(commentDaoMock.findAll()).thenAnswer(new Answer<List<Comment>>() {
			public List<Comment> answer(InvocationOnMock invocation) throws Throwable {
				List<Comment> list= new ArrayList<Comment>();
				list.add(new Comment ());
				list.add(new Comment ());
				list.add(new Comment ());
				return list;
			}
		});
		List<Comment> listFound  = commentService.takeAll();
		assertEquals(3, listFound.size());
	}

	/**
	 * Fidn comments by new id.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void fidnCommentsByNewId() throws ServiceException, DAOException {
		when(commentDaoMock.findByNewsId(any(Long.class))).thenAnswer(new Answer<List<Comment>>() {
			public List<Comment> answer(InvocationOnMock invocation) throws Throwable {
				List<Comment> list= new ArrayList<Comment>();
				list.add(new Comment ());
				list.add(new Comment ());
				list.add(new Comment ());
				return list;
			}
		});
		List<Comment> listFound  = commentService.findByNewsId(1L);
		assertEquals(3, listFound.size());
	}

}
