package com.epam.news.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.news.entity.Author;
import com.epam.news.entity.Comment;
import com.epam.news.entity.News;
import com.epam.news.entity.NewsDTO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;


/**
 * The Class CompleteNewsServiceImplTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class CompleteNewsServiceImplTest {

	/** The news service mock. */
	@Mock
	private NewsServiceImpl newsServiceMock;

	/** The author service mock. */
	@Mock
	private AuthorServiceImpl authorServiceMock;

	/** The tag service mock. */
	@Mock
	private TagServiceImpl tagServiceMock;

	/** The comment service mock. */
	@Mock
	private CommentServiceImpl commentServiceMock;

	/** The Complete news service. */
	@InjectMocks
	private CompleteNewsServiceImpl CompleteNewsService;

	/**
	 * Find complete new.
	 *
	 * @throws ServiceException
	 *             the service exception
	 */
	@Test
    public void findCompleteNew() throws ServiceException{
    	when(newsServiceMock.findById(any(Long.class))).thenAnswer(new Answer<News>() {
    	    @Override
    	    public News answer(InvocationOnMock invocation) throws Throwable {
    		News news = new News();
    	    	news.setNewsId((Long) invocation.getArguments()[0]);
    		return news;
    	    }
    	});
    	when(authorServiceMock.findByNewsId(any(Long.class))).thenAnswer(new Answer<List<Author>>() {
    	    @Override
    	    public List<Author> answer(InvocationOnMock invocation) throws Throwable {
    	    	List<Author> authors = new ArrayList<Author>();
    	    	authors.add(new Author());
    		return authors;
    	    }
    	});
    	when(tagServiceMock.findByNewsId(any(Long.class))).thenAnswer(new Answer<List<Tag>>() {
    	    @Override
    	    public List<Tag> answer(InvocationOnMock invocation) throws Throwable {
    		List<Tag> tags = new ArrayList<Tag>();
    		tags.add(new Tag());
    		tags.add(new Tag());
    	    	return tags;
    	    }
    	});
    	when(commentServiceMock.findByNewsId(any(Long.class))).thenAnswer(new Answer<List<Comment>>() {
    	    @Override
    	    public List<Comment> answer(InvocationOnMock invocation) throws Throwable {
    	    	List<Comment> comments = new ArrayList<Comment>();
    	    	comments.add(new Comment());
    	    	comments.add(new Comment());
    		return comments;
    	    }
    	});
    	News news = new News();
    	news.setNewsId(1L);
    	List<Author> authors = new ArrayList<Author>();
    	authors.add(new Author());
    	List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag());
		tags.add(new Tag());
		List<Comment> comments = new ArrayList<Comment>();
    	comments.add(new Comment());
    	comments.add(new Comment());
    	NewsDTO newDTO = CompleteNewsService.findCompleteNews(1L);
    	assertEquals(news, newDTO.getNews());
    	assertEquals(authors, newDTO.getAuthors());
    	assertEquals(tags, newDTO.getTags());
    	assertEquals(comments, newDTO.getComments());

    }

}
