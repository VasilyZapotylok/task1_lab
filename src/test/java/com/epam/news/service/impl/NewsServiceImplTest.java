package com.epam.news.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.news.dao.impl.NewsDAOImpl;
import com.epam.news.entity.News;
import com.epam.news.entity.SearchCase;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;

/**
 * The Class NewsServiceImplTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplTest {

	/** The news dao mock. */
	@Mock
	private NewsDAOImpl newsDaoMock;

	/** The news service. */
	@InjectMocks
	private NewsServiceImpl newsService;

	/**
	 * Adds the news.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void addNews() throws ServiceException, DAOException {
		when(newsDaoMock.add(any(News.class))).thenAnswer(new Answer<Long>() {
			public Long answer(InvocationOnMock invocation) throws Throwable {
				return 1L;
			}
		});
		News news = getNewsOdject();
		long newsId = newsService.add(news);
		assertEquals(1l, newsId);
	}

	/**
	 * Update news.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void updateNews() throws ServiceException, DAOException {
		News news = getNewsOdject();
		newsService.update(news);
		verify(newsDaoMock).update(news);
	}

	/**
	 * Del news.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void delNews() throws ServiceException, DAOException {
		newsService.deleteById(1L);
		verify(newsDaoMock).deleteById(1L);
	}

	/**
	 * Fidn news by id.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void fidnNewsById() throws ServiceException, DAOException {
		when(newsDaoMock.findById(any(Long.class))).thenAnswer(new Answer<News>() {
			public News answer(InvocationOnMock invocation) throws Throwable {
				News news = getNewsOdject();
				news.setNewsId(((Long) invocation.getArguments()[0]));
				return news;
			}
		});
		News news = getNewsOdject();
		news.setNewsId(1L);
		assertEquals(news, newsService.findById(1L));
	}

	/**
	 * Gets the news odject.
	 *
	 * @return the news odject
	 */
	private News getNewsOdject() {
		News news = new News();
		news.setTitle("title");
		news.setShortText("shortText");
		news.setFullText("fullText");
		news.setCreationDate(new Timestamp(System.currentTimeMillis()));
		news.setModificationDate(new Timestamp(System.currentTimeMillis()));
		return news;
	}

	/**
	 * Find by auhtor id.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 */
	@Test
	public void findByAuhtorId() throws ServiceException, DAOException {
		when(newsDaoMock.findByAuhtorId(any(Long.class))).thenAnswer(new Answer<List<News>>() {
			@Override
			public List<News> answer(InvocationOnMock invocation) throws Throwable {
				List<News> news = new ArrayList<News>();
				news.add(new News());
				news.add(new News());
				news.add(new News());
				return news;
			}
		});
		List<News> news = newsService.findByAuhtorId(1L);
		assertEquals(3, news.size());
	}

	/**
	 * Find by tag id.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 */
	@Test
	public void findByTagId() throws ServiceException, DAOException {
		when(newsDaoMock.findByTagId(any(Long.class))).thenAnswer(new Answer<List<News>>() {
			@Override
			public List<News> answer(InvocationOnMock invocation) throws Throwable {
				List<News> news = new ArrayList<News>();
				news.add(new News());
				news.add(new News());
				news.add(new News());
				return news;
			}
		});

		List<News> news = newsService.findByTagId(1L);
		assertEquals(3, news.size());

	}

	/**
	 * Find news by search case.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 */
	@Test
	public void findNewsBySearchCase() throws ServiceException, DAOException {
		when(newsDaoMock.findNewsBySearchCase(any(SearchCase.class))).thenAnswer(new Answer<List<News>>() {
			@Override
			public List<News> answer(InvocationOnMock invocation) throws Throwable {
				List<News> news = new ArrayList<News>();
				news.add(new News());
				news.add(new News());
				news.add(new News());
				return news;
			}
		});

		List<News> news = newsService.findNewsBySearchCase(new SearchCase());
		assertEquals(3, news.size());

	}

	/**
	 * Count all news.
	 *
	 * @throws ServiceException
	 *             the service exception
	 * @throws DAOException
	 */
	@Test
	public void countAllNews() throws ServiceException, DAOException {
		when(newsDaoMock.countAllNews()).thenAnswer(new Answer<Long>() {
			@Override
			public Long answer(InvocationOnMock invocation) throws Throwable {
				return 100L;
			}
		});
		long amount = newsService.countAllNews();
		assertEquals(100L, amount);

	}


}
