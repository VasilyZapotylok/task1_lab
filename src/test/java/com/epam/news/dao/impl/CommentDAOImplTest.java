package com.epam.news.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.sql.Timestamp;
import java.util.List;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.news.config.ApplicationConfigTest;
import com.epam.news.dao.CommentDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.entity.Comment;
import com.epam.news.entity.News;
import com.epam.news.exception.DAOException;


/**
 * The Class CommentDAOImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationConfigTest.class })
public class CommentDAOImplTest extends BaseConfigDBUnitTest{

	/** The comment dao. */
	@Autowired
	private CommentDAO commentDAO;

	/** The news dao. */
	@Autowired
	private NewsDAO newsDAO;

	/**
	 * Adds the comment.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void addComment() throws DAOException {
		News news = new News();
		news.setTitle("title");
		news.setShortText("shortText");
		news.setFullText("fullText");
		Long newsId = newsDAO.add(news);
		Comment comment = new Comment();
		comment.setCommentText("commentText for test BD");
		comment.setCreationDate(new Timestamp(System.currentTimeMillis()));
		comment.setNewsId(newsId);
		long commentId = commentDAO.add(comment);
		assertNotEquals(0, commentId);
		boolean flag = commentDAO.deleteById(commentId);
		assertNotEquals(false, flag);
	}

	/**
	 * Del comment.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void delComment() throws DAOException {
		boolean flag = commentDAO.deleteById(4L);
		assertNotEquals(false, flag);
	}

	/**
	 * Update comment.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void updateComment() throws DAOException {
		Comment comment = new Comment();
		comment.setCommentText("comment3up");
		comment.setCommentId(3L);
		comment.setNewsId(3L);
		comment.setCreationDate(new Timestamp(System.currentTimeMillis()));
		commentDAO.update(comment);
		Comment comm = commentDAO.findById(3L);
		assertEquals("comment3up", comm.getCommentText());
	}

	/**
	 * Find comment.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void findComment() throws DAOException {
		Comment comment = commentDAO.findById(3L);
		assertNotEquals(null, comment);
	}

	/**
	 * Find by news id.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void findByNewsId() throws DAOException {
		List<Comment> tags = commentDAO.findByNewsId(1L);
		assertEquals(tags.size(), 1);
	}
}
