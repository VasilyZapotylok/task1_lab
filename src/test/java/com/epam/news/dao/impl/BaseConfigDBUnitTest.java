package com.epam.news.dao.impl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.Locale;

import javax.sql.DataSource;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

/**
 * The Class BaseConfigDBUnitTest.
 */
public class BaseConfigDBUnitTest {

	/** The data source. */
	@Autowired
	DataSource dataSource;

	/** The connect java sql. */
	Connection connectJavaSQL;

	/**
	 * Configure data base.
	 *
	 * @throws DatabaseUnitException
	 *             the database unit exception
	 * @throws SQLException
	 *             the SQL exception
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void configureDataBase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), takeDataSetToInsert());
	}

	/**
	 * Release connection.
	 */
	@After
	public void releaseConnection() {
		DataSourceUtils.releaseConnection(connectJavaSQL, dataSource);
	}

	/**
	 * Take connection.
	 *
	 * @return the i database connection
	 * @throws Exception
	 *             the exception
	 */
	public IDatabaseConnection takeConnection() throws Exception {
		Locale.setDefault(new Locale("en", "EN"));
		connectJavaSQL = DataSourceUtils.getConnection(dataSource);
		DatabaseMetaData databaseMetaData = connectJavaSQL.getMetaData();
		IDatabaseConnection connection = new DatabaseConnection(connectJavaSQL,
				databaseMetaData.getUserName().toUpperCase());
		connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());
		return connection;
	}

	/**
	 * Take data set to insert.
	 *
	 * @return the i data set
	 * @throws DataSetException
	 *             the data set exception
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	public IDataSet takeDataSetToInsert() throws DataSetException, FileNotFoundException {
		return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/db_data.xml"));
	}

	/**
	 * Take data set to delete.
	 *
	 * @return the i data set
	 * @throws DataSetException
	 *             the data set exception
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	public IDataSet takeDataSetToDelete() throws DataSetException, FileNotFoundException {
		return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/delete_data.xml"));
	}

}
