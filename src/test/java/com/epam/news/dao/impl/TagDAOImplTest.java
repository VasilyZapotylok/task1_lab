package com.epam.news.dao.impl;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.news.config.ApplicationConfigTest;
import com.epam.news.dao.TagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;

/**
 * The Class TagDAOImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationConfigTest.class })
public class TagDAOImplTest extends BaseConfigDBUnitTest {

	/** The tag dao. */
	@Autowired
	private TagDAO tagDAO;

	/**
	 * Adds the tag.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void addTag() throws DAOException {
		Tag tag = new Tag();
		tag.setTagName("test tag 1");
		long tagId = tagDAO.add(tag);
		assertNotEquals(0, tagId);
		boolean flag = tagDAO.deleteById(tagId);
		assertNotEquals(false, flag);
	}

	/**
	 * Delete tag.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void deleteTag() throws DAOException {
		Tag tag = new Tag();
		tag.setTagName("test tag 2");
		long tagId = tagDAO.add(tag);
		assertNotEquals(0, tagId);
		boolean flag = tagDAO.deleteById(tagId);
		assertNotEquals(false, flag);

	}

	/**
	 * Find by id.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void findById() throws DAOException {
		Tag tag = new Tag();
		tag.setTagId(1L);
		tag.setTagName("tag1");
		assertEquals(tag, tagDAO.findById(1L));
	}

	/**
	 * Update tag.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void updateTag() throws DAOException {
		Tag tag = tagDAO.findById(1L);
		tag.setTagName("updateTag");
		tagDAO.update(tag);
		assertEquals("updateTag", tagDAO.findById(1L).getTagName());
	}

	/**
	 * Find all tags.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void findAllTags() throws DAOException {
		List<Tag> tags = tagDAO.findAll();
		assertEquals(tags.size(), 4);

	}

	/**
	 * Find by news id.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void findByNewsId() throws DAOException {
		List<Tag> tags = tagDAO.findByNewsId(1L);
		assertEquals(tags.size(), 1);
	}
}
