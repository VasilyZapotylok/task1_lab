package com.epam.news.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.sql.Timestamp;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.news.config.ApplicationConfigTest;
import com.epam.news.dao.AuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;

/**
 * The Class AuthorDAOImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationConfigTest.class })
public class AuthorDAOImplTest extends BaseConfigDBUnitTest {

	/** The author dao. */
	@Autowired
	private AuthorDAO authorDAO;

	/**
	 * Adds the author.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void addAuthor() throws DAOException {
		long authorId = authorDAO.add(getAuthor());
		assertNotEquals(0, authorId);
	}

	/**
	 * Update author.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void updateAuthor() throws DAOException {
		Author author = getAuthor();
		author.setAuthorId(4L);
		authorDAO.update(author);
		Author a = authorDAO.findById(4L);
		assertEquals("Test BD Author", a.getAuthorName());
	}

	/**
	 * Find author.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void findAuthor() throws DAOException {
		Author author = authorDAO.findById(4L);
		assertNotEquals(null, author);
	}

	/**
	 * Expire author.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void expireAuthor() throws DAOException {
		Author author = getAuthor();
		Timestamp timastemp = new Timestamp(System.currentTimeMillis());
		author.setExpired(timastemp);
		author.setAuthorId(2L);
		boolean flag = authorDAO.expireAuthor(author);
		assertNotEquals(false, flag);
	}

	/**
	 * Find all author.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void findAllAuthor() throws DAOException {
		List<Author> list = authorDAO.findAll();
		assertEquals(4, list.size());
	}

	/**
	 * Gets the author.
	 *
	 * @return the author
	 */
	private Author getAuthor() {
		Author author = new Author();
		author.setAuthorName("Test BD Author");
		author.setExpired(new Timestamp(System.currentTimeMillis()));
		return author;
	}

}
