package com.epam.news.dao.impl;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.news.config.ApplicationConfigTest;
import com.epam.news.dao.NewsDAO;
import com.epam.news.entity.Author;
import com.epam.news.entity.News;
import com.epam.news.entity.SearchCase;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;

/**
 * The Class NewsDAOImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ApplicationConfigTest.class })
public class NewsDAOImplTest extends BaseConfigDBUnitTest {

	/** The news dao. */
	@Autowired
	private NewsDAO newsDAO;

	/**
	 * Adds the news.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void addNews() throws DAOException {
		long newsId = newsDAO.add(getNewsObject());
		assertNotEquals(0, newsId);
	}

	/**
	 * Del news.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void delNews() throws DAOException {
		boolean flag = newsDAO.deleteById(4L);
		assertNotEquals(false, flag);
	}

	/**
	 * Find news.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void findNews() throws DAOException {
		News news = newsDAO.findById(2L);
		assertEquals("new2", news.getTitle());
	}

	/**
	 * Update news.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void updateNews() throws DAOException {
		News news = getNewsObject();
		news.setNewsId(1L);
		boolean flag = newsDAO.update(news);
		assertNotEquals(false, flag);
	}

	/**
	 * Find news by author id.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void findNewsByAuthorId() throws DAOException {
		List<News> list = newsDAO.findByAuhtorId(2L);
		assertEquals(1, list.size());
	}

	/**
	 * Find news by tag id.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void findNewsByTagId() throws DAOException {
		List<News> list = newsDAO.findByTagId(2L);
		assertEquals(1, list.size());
	}

	/**
	 * Find all news.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void findAllNews() throws DAOException {
		List<News> list = newsDAO.findAll();
		assertEquals(4, list.size());
	}

	/**
	 * Gets the news object.
	 *
	 * @return the news object
	 */
	private News getNewsObject() {
		News news = new News();
		news.setTitle("title");
		news.setShortText("shortText");
		news.setFullText("fullText");
		news.setCreationDate(new Timestamp(System.currentTimeMillis()));
		news.setModificationDate(new Timestamp(System.currentTimeMillis()));
		return news;
	}

	/**
	 * Adds the tags to news.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void addTagsToNews() throws DAOException {

		List<Long> list = new ArrayList<Long>();
		list.add(2L);
		list.add(3L);
		boolean flag = newsDAO.addNewsTagList(1L, list);
		assertNotEquals(false, flag);
	}

	/**
	 * Adds the autors to news.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void addAutorsToNews() throws DAOException {

		List<Long> list = new ArrayList<Long>();
		list.add(2L);
		list.add(3L);
		boolean flag = newsDAO.addNewsAuthorList(1L, list);
		assertNotEquals(false, flag);
	}

	/**
	 * Count all news.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void countAllNews() throws DAOException {
		long amount = newsDAO.countAllNews();
		assertEquals(4l, amount);
	}

	/**
	 * Find news by search case.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void findNewsBySearchCase() throws DAOException {
		Author author = new Author();
		author.setAuthorName("author1");
		List<Tag> list = new ArrayList<Tag>();
		Tag tag1 = new Tag();
		tag1.setTagName("tag1");
		Tag tag2 = new Tag();
		tag2.setTagName("tag2");
		list.add(tag1);
		list.add(tag2);
		SearchCase searchCase = new SearchCase();
		searchCase.setAuthor(author);
		searchCase.setTags(list);
		List<News> newsList = newsDAO.findNewsBySearchCase(searchCase);
		assertEquals(4, newsList.size());
	}

	/**
	 * Find last news by amount.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Test
	public void findLastNewsByAmount() throws DAOException{
		List<News> list = newsDAO.findLastNewsByAmount(2L);
		assertEquals(2, list.size());
	}

}
