package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;

/**
 * The Interface AuthorDAO.
 */
public interface AuthorDAO extends DAO<Author> {

	/**
	 * Expire author.
	 *
	 * @param author
	 *            the author
	 * @return true, if successful
	 * @throws DAOException
	 *             the DAO exception
	 */
	boolean expireAuthor(Author author) throws DAOException;

	/**
	 * Find by news id.
	 *
	 * @param newsId
	 *            the news id
	 * @return the list
	 * @throws DAOException
	 *             the DAO exception
	 */
	List<Author> findByNewsId(Long newsId) throws DAOException;

}
