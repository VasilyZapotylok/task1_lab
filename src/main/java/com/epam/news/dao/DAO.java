package com.epam.news.dao;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.epam.news.exception.DAOException;

/**
 * The Interface DAO.
 *
 * @param <T>
 *            the generic type
 */
public interface DAO<T> {

	/**
	 * Adds the.
	 *
	 * @param t
	 *            the t
	 * @return the long
	 * @throws DAOException
	 *             the DAO exception
	 */
	Long add(T t) throws DAOException;

	/**
	 * Find by id.
	 *
	 * @param id
	 *            the id
	 * @return the t
	 * @throws DAOException
	 *             the DAO exception
	 */
	T findById(Long id) throws DAOException;

	/**
	 * Update.
	 *
	 * @param t
	 *            the t
	 * @return true, if successful
	 * @throws DAOException
	 *             the DAO exception
	 */
	boolean update(T t) throws DAOException;

	/**
	 * Delete by id.
	 *
	 * @param id
	 *            the id
	 * @return true, if successful
	 * @throws DAOException
	 *             the DAO exception
	 */
	boolean deleteById(Long id) throws DAOException;

	/**
	 * Find all.
	 *
	 * @return the list
	 * @throws DAOException
	 *             the DAO exception
	 */
	List<T> findAll() throws DAOException;

	/**
	 * Close.
	 *
	 * @param statement
	 *            the statement
	 * @throws DAOException
	 *             the DAO exception
	 */
	default void close(Statement statement) throws DAOException {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				throw new DAOException(e);
			}
		}
	}

}
