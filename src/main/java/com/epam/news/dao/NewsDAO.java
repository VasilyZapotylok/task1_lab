package com.epam.news.dao;

import java.util.List;


import com.epam.news.entity.News;
import com.epam.news.entity.SearchCase;
import com.epam.news.exception.DAOException;

/**
 * The Interface NewsDAO.
 */
public interface NewsDAO  extends DAO<News> {


    /**
	 * Find by auhtor id.
	 *
	 * @param authorId
	 *            the author id
	 * @return the list
	 * @throws DAOException
	 *             the DAO exception
	 */
    List<News> findByAuhtorId(Long authorId) throws DAOException;

    /**
	 * Find by tag id.
	 *
	 * @param tagId
	 *            the tag id
	 * @return the list
	 * @throws DAOException
	 *             the DAO exception
	 */
    List<News> findByTagId(Long tagId) throws DAOException;

    /**
	 * Find news by search case.
	 *
	 * @param searchCase
	 *            the search case
	 * @return the list
	 * @throws DAOException
	 *             the DAO exception
	 */
    List<News> findNewsBySearchCase(SearchCase searchCase) throws DAOException;

    /**
	 * Adds the news author list.
	 *
	 * @param newsId
	 *            the news id
	 * @param authorsId
	 *            the authors id
	 * @return true, if successful
	 * @throws DAOException
	 *             the DAO exception
	 */
    boolean addNewsAuthorList(Long newsId, List<Long> authorsId) throws DAOException;

    /**
	 * Adds the news tag list.
	 *
	 * @param newsId
	 *            the news id
	 * @param tagsId
	 *            the tags id
	 * @return true, if successful
	 * @throws DAOException
	 *             the DAO exception
	 */
    boolean addNewsTagList(Long newsId, List<Long> tagsId) throws DAOException;

    /**
	 * Del all comment by news id.
	 *
	 * @param newsId
	 *            the news id
	 * @return true, if successful
	 * @throws DAOException
	 *             the DAO exception
	 */
    boolean delAllCommentByNewsId(Long newsId) throws DAOException;

    /**
	 * Del news author list.
	 *
	 * @param newsId
	 *            the news id
	 * @return true, if successful
	 * @throws DAOException
	 *             the DAO exception
	 */
    boolean delNewsAuthorList(Long newsId) throws DAOException;

    /**
	 * Del news tag list.
	 *
	 * @param newsId
	 *            the news id
	 * @return true, if successful
	 * @throws DAOException
	 *             the DAO exception
	 */
    boolean delNewsTagList(Long newsId) throws DAOException;

    /**
	 * Count all news.
	 *
	 * @return the long
	 * @throws DAOException
	 *             the DAO exception
	 */
    Long countAllNews() throws DAOException;

    /**
	 * Find last news by amount.
	 *
	 * @param amount
	 *            the amount
	 * @return the list
	 * @throws DAOException
	 *             the DAO exception
	 */
    List<News> findLastNewsByAmount(Long amount) throws DAOException;



}
