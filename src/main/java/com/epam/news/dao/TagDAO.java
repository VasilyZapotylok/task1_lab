package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;

/**
 * The Interface TagDAO.
 */
public interface TagDAO extends DAO<Tag> {

	/**
	 * Find by news id.
	 *
	 * @param newsId
	 *            the news id
	 * @return the list
	 * @throws DAOException
	 *             the DAO exception
	 */
	List<Tag> findByNewsId(Long newsId) throws DAOException;

}
