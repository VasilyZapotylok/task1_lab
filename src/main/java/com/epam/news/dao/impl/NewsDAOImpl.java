package com.epam.news.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.news.dao.NewsDAO;
import com.epam.news.entity.News;
import com.epam.news.entity.SearchCase;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;

/**
 * The Class NewsDAOImpl.
 */
@Repository
public class NewsDAOImpl implements NewsDAO {

	/** The data source. */
	@Autowired
	private DataSource dataSource;

	/** The Constant SQL_ADD_NEWS. */
	private static final String SQL_ADD_NEWS = "INSERT INTO NEWS (TITLE, SHOT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (?,?,?,?,?)";

	/** The Constant SQL_FIND_NEWS_BY_ID. */
	private static final String SQL_FIND_NEWS_BY_ID = "SELECT NEWS_ID, TITLE, SHOT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE NEWS_ID = ?";

	/** The Constant SQL_UPDATE_NEWS. */
	private static final String SQL_UPDATE_NEWS = "UPDATE NEWS SET TITLE = ?, SHOT_TEXT = ?, FULL_TEXT = ?, CREATION_DATE = ?, MODIFICATION_DATE = ? WHERE NEWS_ID = ?";

	/** The Constant SQL_DELETE_NEWS. */
	private static final String SQL_DELETE_NEWS = "DELETE FROM NEWS WHERE NEWS_ID = ?";

	/** The Constant SQL_FIND_ALL_NEWS. */
	private static final String SQL_FIND_ALL_NEWS = "SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHOT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS ORDER BY MODIFICATION_DATE DESC";

	/** The Constant SQL_FIND_NEWS_BY_AUTHOR_ID. */
	private static final String SQL_FIND_NEWS_BY_AUTHOR_ID = "SELECT NEWS.NEWS_ID, TITLE, SHOT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE FROM NEWS JOIN NEWS_AUTHOR ON (NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID)  WHERE AUTHOR_ID = ? ORDER BY MODIFICATION_DATE DESC";

	/** The Constant SQL_FIND_NEWS_BY_TAG_ID. */
	private static final String SQL_FIND_NEWS_BY_TAG_ID = "SELECT NEWS.NEWS_ID, TITLE, SHOT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE FROM NEWS JOIN NEWS_TAG ON (NEWS.NEWS_ID = NEWS_TAG.NEWS_ID) WHERE TAG_ID = ? ORDER BY MODIFICATION_DATE DESC";

	/** The Constant SQL_ADD_NEWS_AUTHOR. */
	private static final String SQL_ADD_NEWS_AUTHOR = "INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (?,?)";

	/** The Constant SQL_ADD_NEWS_TAG. */
	private static final String SQL_ADD_NEWS_TAG = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (?,?)";

	/** The Constant SQL_SEARCH_NEWS. */
	private static final String SQL_SEARCH_NEWS = "SELECT  NEWS.NEWS_ID, TITLE, SHOT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE FROM NEWS JOIN NEWS_AUTHOR ON (NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID) JOIN AUTHOR ON (NEWS_AUTHOR.AUTHOR_ID = AUTHOR.AUTHOR_ID) JOIN NEWS_TAG ON (NEWS.NEWS_ID = NEWS_TAG.NEWS_ID) JOIN TAG ON (NEWS_TAG.TAG_ID = TAG.TAG_ID) WHERE";

	/** The Constant SQL_COUNT_ALL_NEWS. */
	private static final String SQL_COUNT_ALL_NEWS = "SELECT COUNT(*) FROM NEWS";

	/** The Constant SQL_LAST_NEWS_BY_AMOUNT. */
	private static final String SQL_LAST_NEWS_BY_AMOUNT = "SELECT NEWS_ID, TITLE, SHOT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM  (SELECT NEWS_ID, TITLE, SHOT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS ORDER BY MODIFICATION_DATE DESC) WHERE ROWNUM <= ?";

	/** The Constant SQL_DELETE_ALL_COMMENTS_BY_NEWS_ID. */
	private static final String SQL_DELETE_ALL_COMMENTS_BY_NEWS_ID = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";

	/** The Constant SQL_DELETE_TAGS_BY_NEWS_ID. */
	private static final String SQL_DELETE_TAGS_BY_NEWS_ID = "DELETE FROM NEWS_TAG WHERE NEWS_ID = ?";

	/** The Constant SQL_DELETE_AUTHORS_BY_NEWS_ID. */
	private static final String SQL_DELETE_AUTHORS_BY_NEWS_ID = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID = ?";

	/** The Constant COLUMN_NAME_NEWS_ID. */
	private static final String COLUMN_NAME_NEWS_ID = "NEWS_ID";

	/** The Constant COLUMN_NAME_NEWS_TITLE. */
	private static final String COLUMN_NAME_NEWS_TITLE = "TITLE";

	/** The Constant COLUMN_NAME_NEWS_SHOT_TEXT. */
	private static final String COLUMN_NAME_NEWS_SHOT_TEXT = "SHOT_TEXT";

	/** The Constant COLUMN_NAME_NEWS_FULL_TEXT. */
	private static final String COLUMN_NAME_NEWS_FULL_TEXT = "FULL_TEXT";

	/** The Constant COLUMN_NAME_NEWS_CREATION_DATE. */
	private static final String COLUMN_NAME_NEWS_CREATION_DATE = "CREATION_DATE";

	/** The Constant COLUMN_NAME_NEWS_MODIFICATION_DATE. */
	private static final String COLUMN_NAME_NEWS_MODIFICATION_DATE = "MODIFICATION_DATE";

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#add(java.lang.Object)
	 */
	@Override
	public Long add(News news) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		String[] columnNames = { COLUMN_NAME_NEWS_ID };
		Long newsId = null;
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_ADD_NEWS, columnNames);
			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getShortText());
			preparedStatement.setString(3, news.getFullText());
			preparedStatement.setTimestamp(4, news.getCreationDate());
			preparedStatement.setTimestamp(5, news.getModificationDate());
			preparedStatement.executeUpdate();
			try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
				if (resultSet.next()) {
					newsId = resultSet.getLong(1);
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return newsId;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#findById(java.lang.Long)
	 */
	@Override
	public News findById(Long newsId) throws DAOException {
		News news = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_FIND_NEWS_BY_ID);
			preparedStatement.setLong(1, newsId);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				news = parseResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return news;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#update(java.lang.Object)
	 */
	@Override
	public boolean update(News news) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		PreparedStatement preparedStatement = null;
		boolean isDone = false;
		try {
			preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS);
			preparedStatement.setString(1, news.getTitle());
			preparedStatement.setString(2, news.getShortText());
			preparedStatement.setString(3, news.getFullText());
			preparedStatement.setTimestamp(4, news.getCreationDate());
			preparedStatement.setTimestamp(5, news.getModificationDate());
			preparedStatement.setLong(6, news.getNewsId());
			int count = preparedStatement.executeUpdate();
			if (count == 1) {
				isDone = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return isDone;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#deleteById(java.lang.Long)
	 */
	@Override
	public boolean deleteById(Long newsId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		PreparedStatement preparedStatement = null;
		boolean isDone = false;
		try {
			preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS);
			preparedStatement.setLong(1, newsId);
			int count = preparedStatement.executeUpdate();
			if (count == 1) {
				isDone = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return isDone;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#findAll()
	 */
	@Override
	public List<News> findAll() throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		List<News> newsList = new ArrayList<News>();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_FIND_ALL_NEWS);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				newsList.add(parseResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return newsList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.NewsDAO#findByAuhtorId(java.lang.Long)
	 */
	@Override
	public List<News> findByAuhtorId(Long authorId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		List<News> newsList = new ArrayList<News>();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_FIND_NEWS_BY_AUTHOR_ID);
			preparedStatement.setLong(1, authorId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				newsList.add(parseResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return newsList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.NewsDAO#findByTagId(java.lang.Long)
	 */
	@Override
	public List<News> findByTagId(Long tagId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		List<News> newsList = new ArrayList<News>();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_FIND_NEWS_BY_TAG_ID);
			preparedStatement.setLong(1, tagId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				newsList.add(parseResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return newsList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.NewsDAO#findNewsBySearchCase(com.epam.news.entity.
	 * SearchCase)
	 */
	@Override
	public List<News> findNewsBySearchCase(SearchCase searchCase) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		String query = buildSearchQuery(searchCase);
		List<News> newsList = new ArrayList<News>();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(query);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				newsList.add(parseResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return newsList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.NewsDAO#addNewsAuthorList(java.lang.Long,
	 * java.util.List)
	 */
	@Override
	public boolean addNewsAuthorList(Long newsId, List<Long> authorsId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		PreparedStatement preparedStatement = null;
		boolean isDone = false;
		try {
			preparedStatement = connection.prepareStatement(SQL_ADD_NEWS_AUTHOR);
			connection.setAutoCommit(false);
			Iterator<Long> iterator = authorsId.iterator();
			Long authorId;
			while (iterator.hasNext()) {
				authorId = iterator.next();
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, authorId);
				preparedStatement.addBatch();
			}
			int[] count = preparedStatement.executeBatch();
			connection.commit();
			if (count.length > 0) {
				isDone = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return isDone;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.NewsDAO#addNewsTagList(java.lang.Long,
	 * java.util.List)
	 */
	@Override
	public boolean addNewsTagList(Long newsId, List<Long> tagsId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		PreparedStatement preparedStatement = null;
		boolean isDone = false;
		try {
			preparedStatement = connection.prepareStatement(SQL_ADD_NEWS_TAG);
			connection.setAutoCommit(false);
			Iterator<Long> iterator = tagsId.iterator();
			Long tagId;
			while (iterator.hasNext()) {
				tagId = iterator.next();
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tagId);
				preparedStatement.addBatch();
			}
			int[] count = preparedStatement.executeBatch();
			connection.commit();
			if (count.length > 0) {
				isDone = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return isDone;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.NewsDAO#countAllNews()
	 */
	@Override
	public Long countAllNews() throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		Long newsCounter = null;
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_COUNT_ALL_NEWS);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				newsCounter = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return newsCounter;
	}

	/**
	 * Parses the result set.
	 *
	 * @param resultSet
	 *            the result set
	 * @return the news
	 * @throws DAOException
	 *             the DAO exception
	 */
	private News parseResultSet(ResultSet resultSet) throws DAOException {
		News news = new News();
		try {
			news.setNewsId(resultSet.getLong(COLUMN_NAME_NEWS_ID));
			news.setTitle(resultSet.getString(COLUMN_NAME_NEWS_TITLE));
			news.setShortText(resultSet.getString(COLUMN_NAME_NEWS_SHOT_TEXT));
			news.setFullText(resultSet.getString(COLUMN_NAME_NEWS_FULL_TEXT));
			news.setCreationDate(resultSet.getTimestamp(COLUMN_NAME_NEWS_CREATION_DATE));
			news.setModificationDate(resultSet.getTimestamp(COLUMN_NAME_NEWS_MODIFICATION_DATE));
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		return news;
	}

	/**
	 * Builds the search query.
	 *
	 * @param searchCase
	 *            the search case
	 * @return the string
	 */
	private String buildSearchQuery(SearchCase searchCase) {
		StringBuilder query = new StringBuilder(SQL_SEARCH_NEWS);
		String authorName = searchCase.getAuthor().getAuthorName();
		List<Tag> tagList = searchCase.getTags();
		if (authorName != null) {
			query.append(" AUTHOR.AUTHOR_NAME IN ('");
			query.append(authorName);
			query.append("')");
		}
		if (!tagList.isEmpty()) {
			if (authorName != null) {
				query.append(" OR ");
			}
			query.append(" TAG.TAG_NAME IN (");
			Iterator<Tag> iterator = tagList.iterator();
			while (iterator.hasNext()) {
				query.append("'" + iterator.next().getTagName());
				if (iterator.hasNext()) {
					query.append("', ");
				} else {
					query.append("')");
				}
			}
		}

		return query.toString();

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.NewsDAO#findLastNewsByAmount(java.lang.Long)
	 */
	@Override
	public List<News> findLastNewsByAmount(Long amount) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		List<News> newsList = new ArrayList<News>();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_LAST_NEWS_BY_AMOUNT);
			preparedStatement.setLong(1, amount);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				newsList.add(parseResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return newsList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.NewsDAO#delAllCommentByNewsId(java.lang.Long)
	 */
	@Override
	public boolean delAllCommentByNewsId(Long newsId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		PreparedStatement preparedStatement = null;
		boolean isDone = false;
		try {
			preparedStatement = connection.prepareStatement(SQL_DELETE_ALL_COMMENTS_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			int count = preparedStatement.executeUpdate();
			if (count == 1) {
				isDone = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return isDone;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.NewsDAO#delNewsAuthorList(java.lang.Long)
	 */
	@Override
	public boolean delNewsAuthorList(Long newsId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		PreparedStatement preparedStatement = null;
		boolean isDone = false;
		try {
			preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHORS_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			int count = preparedStatement.executeUpdate();
			if (count == 1) {
				isDone = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return isDone;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.NewsDAO#delNewsTagList(java.lang.Long)
	 */
	@Override
	public boolean delNewsTagList(Long newsId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		PreparedStatement preparedStatement = null;
		boolean isDone = false;
		try {
			preparedStatement = connection.prepareStatement(SQL_DELETE_TAGS_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			int count = preparedStatement.executeUpdate();
			if (count == 1) {
				isDone = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return isDone;
	}

}
