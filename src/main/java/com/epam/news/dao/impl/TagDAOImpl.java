package com.epam.news.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.news.dao.TagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;

/**
 * The Class TagDAOImpl.
 */
@Repository
public class TagDAOImpl implements TagDAO {

	/** The data source. */
	@Autowired
	private DataSource dataSource;

	/** The Constant SQL_ADD_TAG. */
	private static final String SQL_ADD_TAG = "INSERT INTO TAG (TAG_NAME) VALUES (?)";

	/** The Constant SQL_UPDATE_TAG. */
	private static final String SQL_UPDATE_TAG = "UPDATE TAG SET TAG_NAME = ? WHERE TAG_ID = ?";

	/** The Constant SQL_DELETE_TAG. */
	private static final String SQL_DELETE_TAG = "DELETE FROM TAG WHERE TAG_ID = ?";

	/** The Constant SQL_FIND_TAG_BY_ID. */
	private static final String SQL_FIND_TAG_BY_ID = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_ID = ?";

	/** The Constant SQL_FIND_ALL_TAGS. */
	private static final String SQL_FIND_ALL_TAGS = "SELECT TAG_ID, TAG_NAME FROM TAG";

	/** The Constant SQL_FIND_TAGS_BY_NEWS_ID. */
	private static final String SQL_FIND_TAGS_BY_NEWS_ID = "SELECT DISTINCT TAG.TAG_ID, TAG_NAME FROM TAG JOIN NEWS_TAG ON (TAG.TAG_ID = NEWS_TAG.TAG_ID) WHERE NEWS_ID = ?";

	/** The Constant COLUMN_NAME_TAGS_ID. */
	private static final String COLUMN_NAME_TAGS_ID = "TAG_ID";

	/** The Constant COLUMN_NAME_TAGS_TAG_NAME. */
	private static final String COLUMN_NAME_TAGS_TAG_NAME = "TAG_NAME";

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#add(java.lang.Object)
	 */
	@Override
	public Long add(Tag tag) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		String[] columnNames = { COLUMN_NAME_TAGS_ID };
		Long tagId = null;
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_ADD_TAG, columnNames);
			preparedStatement.setString(1, tag.getTagName());
			preparedStatement.executeUpdate();
			try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
				if (resultSet.next()) {
					tagId = resultSet.getLong(1);
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return tagId;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#findById(java.lang.Long)
	 */
	@Override
	public Tag findById(Long tagId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		Tag tag = null;
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_FIND_TAG_BY_ID);
			preparedStatement.setLong(1, tagId);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				tag = parseResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return tag;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#update(java.lang.Object)
	 */
	@Override
	public boolean update(Tag tag) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		PreparedStatement preparedStatement = null;
		boolean isDone = false;
		try {
			preparedStatement = connection.prepareStatement(SQL_UPDATE_TAG);
			preparedStatement.setString(1, tag.getTagName());
			preparedStatement.setLong(2, tag.getTagId());
			int count = preparedStatement.executeUpdate();
			if (count == 1) {
				isDone = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return isDone;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#deleteById(java.lang.Long)
	 */
	@Override
	public boolean deleteById(Long tagId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		PreparedStatement preparedStatement = null;
		boolean isDone = false;
		try {
			preparedStatement = connection.prepareStatement(SQL_DELETE_TAG);
			preparedStatement.setLong(1, tagId);
			int count = preparedStatement.executeUpdate();
			if (count == 1) {
				isDone = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return isDone;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#findAll()
	 */
	@Override
	public List<Tag> findAll() throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		List<Tag> tags = new ArrayList<>();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_FIND_ALL_TAGS);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tags.add(parseResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return tags;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.TagDAO#findByNewsId(java.lang.Long)
	 */
	@Override
	public List<Tag> findByNewsId(Long newsId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		List<Tag> tags = new ArrayList<>();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_FIND_TAGS_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				tags.add(parseResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return tags;
	}

	/**
	 * Parses the result set.
	 *
	 * @param resultSet
	 *            the result set
	 * @return the tag
	 * @throws DAOException
	 *             the DAO exception
	 */
	private Tag parseResultSet(ResultSet resultSet) throws DAOException {
		Tag tag = new Tag();
		try {
			tag.setTagId(resultSet.getLong(COLUMN_NAME_TAGS_ID));
			tag.setTagName(resultSet.getString(COLUMN_NAME_TAGS_TAG_NAME));
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		return tag;
	}
}
