package com.epam.news.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.news.dao.CommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;

/**
 * The Class CommentDAOImpl.
 */
@Repository
public class CommentDAOImpl implements CommentDAO {

	/** The data source. */
	@Autowired
	private DataSource dataSource;

	/** The Constant SQL_ADD_COMMENT. */
	private static final String SQL_ADD_COMMENT = "INSERT INTO COMMENTS (NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES (?,?,?)";

	/** The Constant SQL_UPDATE_COMMENT. */
	private static final String SQL_UPDATE_COMMENT = "UPDATE COMMENTS SET NEWS_ID = ?, COMMENT_TEXT = ?, CREATION_DATE = ? WHERE COMMENT_ID = ?";

	/** The Constant SQL_DELETE_COMMENT. */
	private static final String SQL_DELETE_COMMENT = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";

	/** The Constant SQL_FIND_COMMENT_BY_ID. */
	private static final String SQL_FIND_COMMENT_BY_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE COMMENT_ID = ?";

	/** The Constant SQL_FIND_ALL_COMMENTS. */
	private static final String SQL_FIND_ALL_COMMENTS = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS ORDER BY CREATION_DATE DESC";

	/** The Constant SQL_FIND_COMMENTS_BY_NEWS_ID. */
	private static final String SQL_FIND_COMMENTS_BY_NEWS_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE NEWS_ID = ? ORDER BY CREATION_DATE DESC";

	/** The Constant COLUMN_NAME_COMMENTS_ID. */
	private static final String COLUMN_NAME_COMMENTS_ID = "COMMENT_ID";

	/** The Constant COLUMN_NAME_COMMENTS_NEWS_ID. */
	private static final String COLUMN_NAME_COMMENTS_NEWS_ID = "NEWS_ID";

	/** The Constant COLUMN_NAME_COMMENTS_COMMENT_TEXT. */
	private static final String COLUMN_NAME_COMMENTS_COMMENT_TEXT = "COMMENT_TEXT";

	/** The Constant COLUMN_NAME_COMMENTS_CREATION_DATE. */
	private static final String COLUMN_NAME_COMMENTS_CREATION_DATE = "CREATION_DATE";

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#add(java.lang.Object)
	 */
	public Long add(Comment comment) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		String[] columnNames = { COLUMN_NAME_COMMENTS_ID };
		Long commentId = null;
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_ADD_COMMENT, columnNames);
			preparedStatement.setLong(1, comment.getNewsId());
			preparedStatement.setString(2, comment.getCommentText());
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			preparedStatement.setTimestamp(3, timestamp);
			preparedStatement.executeUpdate();
			try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
				if (resultSet.next()) {
					commentId = resultSet.getLong(1);
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return commentId;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#findById(java.lang.Long)
	 */
	@Override
	public Comment findById(Long commentId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		Comment comment = null;
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_FIND_COMMENT_BY_ID);
			preparedStatement.setLong(1, commentId);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				comment = parseResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return comment;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#update(java.lang.Object)
	 */
	@Override
	public boolean update(Comment comment) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		PreparedStatement preparedStatement = null;
		boolean isDone = false;
		try {
			preparedStatement = connection.prepareStatement(SQL_UPDATE_COMMENT);
			preparedStatement.setLong(1, comment.getNewsId());
			preparedStatement.setString(2, comment.getCommentText());
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			preparedStatement.setTimestamp(3, timestamp);
			preparedStatement.setLong(4, comment.getCommentId());
			int count = preparedStatement.executeUpdate();
			if (count == 1) {
				isDone = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return isDone;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#deleteById(java.lang.Long)
	 */
	@Override
	public boolean deleteById(Long commentId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		PreparedStatement preparedStatement = null;
		boolean isDone = false;
		try {
			preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT);
			preparedStatement.setLong(1, commentId);
			int count = preparedStatement.executeUpdate();
			if (count == 1) {
				isDone = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return isDone;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#findAll()
	 */
	@Override
	public List<Comment> findAll() throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		List<Comment> comments = new ArrayList<Comment>();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_FIND_ALL_COMMENTS);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				comments.add(parseResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return comments;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.CommentDAO#findByNewsId(java.lang.Long)
	 */
	@Override
	public List<Comment> findByNewsId(Long newsId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		List<Comment> comments = new ArrayList<Comment>();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_FIND_COMMENTS_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				comments.add(parseResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return comments;
	}

	/**
	 * Parses the result set.
	 *
	 * @param resultSet
	 *            the result set
	 * @return the comment
	 * @throws DAOException
	 *             the DAO exception
	 */
	private Comment parseResultSet(ResultSet resultSet) throws DAOException {
		Comment comment = new Comment();
		try {
			comment.setCommentId(resultSet.getLong(COLUMN_NAME_COMMENTS_ID));
			comment.setNewsId(resultSet.getLong(COLUMN_NAME_COMMENTS_NEWS_ID));
			comment.setCommentText(resultSet.getString(COLUMN_NAME_COMMENTS_COMMENT_TEXT));
			comment.setCreationDate(resultSet.getTimestamp(COLUMN_NAME_COMMENTS_CREATION_DATE));
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		return comment;
	}

}
