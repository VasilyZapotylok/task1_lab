package com.epam.news.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;

/**
 * The Class AuthorDAOImpl.
 */
@Repository
public class AuthorDAOImpl implements AuthorDAO {

	/** The Constant SQL_ADD_AUTHOR. */
	private static final String SQL_ADD_AUTHOR = "INSERT INTO AUTHOR (AUTHOR_NAME) VALUES (?)";

	/** The Constant SQL_UPDATE_AUTHOR. */
	private static final String SQL_UPDATE_AUTHOR = "UPDATE AUTHOR SET AUTHOR_NAME = ?, EXPIRED = ? WHERE AUTHOR_ID = ?";

	/** The Constant SQL_FIND_AUTHOR_BY_ID. */
	private static final String SQL_FIND_AUTHOR_BY_ID = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_ID = ?";

	/** The Constant SQL_FIND_ALL_AUTHORS. */
	private static final String SQL_FIND_ALL_AUTHORS = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR";

	/** The Constant SQL_SET_EXPIRED. */
	private static final String SQL_SET_EXPIRED = "UPDATE AUTHOR SET EXPIRED = ? WHERE AUTHOR_ID = ?";

	/** The Constant SQL_FIND_AUTHORS_BY_NEWS_ID. */
	private static final String SQL_FIND_AUTHORS_BY_NEWS_ID = "SELECT DISTINCT AUTHOR.AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM NEWS_AUTHOR JOIN AUTHOR ON (NEWS_AUTHOR.AUTHOR_ID = AUTHOR.AUTHOR_ID) WHERE NEWS_ID = ?";

	/** The Constant COLUNM_NAME_AUTHOR_ID. */
	private static final String COLUNM_NAME_AUTHOR_ID = "AUTHOR_ID";

	/** The Constant COLUNM_NAME_AUTHOR_NAME. */
	private static final String COLUNM_NAME_AUTHOR_NAME = "AUTHOR_NAME";

	/** The Constant COLUNM_NAME_AUTHOR_EXPIRED. */
	private static final String COLUNM_NAME_AUTHOR_EXPIRED = "EXPIRED";

	/** The data source. */
	@Autowired
	private DataSource dataSource;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#add(java.lang.Object)
	 */
	public Long add(Author author) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		String[] columnNames = { COLUNM_NAME_AUTHOR_ID };
		Long authorId = null;
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_ADD_AUTHOR, columnNames);
			preparedStatement.setString(1, author.getAuthorName());
			preparedStatement.executeUpdate();
			try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
				if (resultSet.next()) {
					authorId = resultSet.getLong(1);
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return authorId;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#findById(java.lang.Long)
	 */
	public Author findById(Long authorId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		Author author = null;
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_FIND_AUTHOR_BY_ID);
			preparedStatement.setLong(1, authorId);
			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				author = parseResultSet(resultSet);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return author;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#update(java.lang.Object)
	 */
	public boolean update(Author author) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		PreparedStatement preparedStatement = null;
		boolean isDone = false;
		try {
			preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR);
			preparedStatement.setString(1, author.getAuthorName());
			preparedStatement.setTimestamp(2, author.getExpired());
			preparedStatement.setLong(3, author.getAuthorId());
			int count = preparedStatement.executeUpdate();
			if (count == 1) {
				isDone = true;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return isDone;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#deleteById(java.lang.Long)
	 */
	public boolean deleteById(Long id) throws DAOException {
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.DAO#findAll()
	 */
	public List<Author> findAll() throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		List<Author> authors = new ArrayList<>();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_FIND_ALL_AUTHORS);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				authors.add(parseResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return authors;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.epam.news.dao.AuthorDAO#expireAuthor(com.epam.news.entity.Author)
	 */
	public boolean expireAuthor(Author author) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		PreparedStatement preparedStatement = null;
		boolean isDone = false;
		try {
			preparedStatement = connection.prepareStatement(SQL_SET_EXPIRED);
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			preparedStatement.setTimestamp(1, timestamp);
			preparedStatement.setLong(2, author.getAuthorId());
			int count = preparedStatement.executeUpdate();
			if (count == 1) {
				isDone = true;
			}
			author.setExpired(timestamp);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return isDone;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.epam.news.dao.AuthorDAO#findByNewsId(java.lang.Long)
	 */
	public List<Author> findByNewsId(Long newsId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		List<Author> authors = new ArrayList<>();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(SQL_FIND_AUTHORS_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				authors.add(parseResultSet(resultSet));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			close(preparedStatement);
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return authors;
	}

	/**
	 * Parses the result set.
	 *
	 * @param resultSet
	 *            the result set
	 * @return the author
	 * @throws DAOException
	 *             the DAO exception
	 */
	private Author parseResultSet(ResultSet resultSet) throws DAOException {
		Author author = new Author();
		try {
			author.setAuthorId(resultSet.getLong(COLUNM_NAME_AUTHOR_ID));
			author.setAuthorName(resultSet.getString(COLUNM_NAME_AUTHOR_NAME));
			author.setExpired(resultSet.getTimestamp(COLUNM_NAME_AUTHOR_EXPIRED));
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		return author;
	}

}
