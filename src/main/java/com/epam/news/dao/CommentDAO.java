package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;

/**
 * The Interface CommentDAO.
 */
public interface CommentDAO extends DAO<Comment> {

	/**
	 * Find by news id.
	 *
	 * @param newsId
	 *            the news id
	 * @return the list
	 * @throws DAOException
	 *             the DAO exception
	 */
	List<Comment> findByNewsId(Long newsId) throws DAOException;

}
