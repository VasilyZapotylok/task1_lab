package com.epam.news.service;

import java.util.List;

import com.epam.news.exception.ServiceException;

/**
 * The Interface Service.
 *
 * @param <T>
 *            the generic type
 */
public interface Service<T> {

    /**
	 * Adds the.
	 *
	 * @param t
	 *            the t
	 * @return the long
	 * @throws ServiceException
	 *             the service exception
	 */
    Long add(T t) throws ServiceException;

    /**
	 * Find by id.
	 *
	 * @param id
	 *            the id
	 * @return the t
	 * @throws ServiceException
	 *             the service exception
	 */
    T findById(Long id) throws ServiceException;

    /**
	 * Update.
	 *
	 * @param t
	 *            the t
	 * @return true, if successful
	 * @throws ServiceException
	 *             the service exception
	 */
    boolean update(T t) throws ServiceException;

    /**
	 * Delete by id.
	 *
	 * @param id
	 *            the id
	 * @return true, if successful
	 * @throws ServiceException
	 *             the service exception
	 */
    boolean deleteById(Long id) throws ServiceException;

    /**
	 * Take all.
	 *
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
    List<T> takeAll() throws ServiceException;

}
