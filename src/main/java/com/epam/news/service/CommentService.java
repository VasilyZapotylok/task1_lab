package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.Comment;
import com.epam.news.exception.ServiceException;


/**
 * The Interface CommentService.
 */
public interface CommentService extends Service<Comment>{

	/**
	 * Find by new id.
	 *
	 * @param newId
	 *            the new id
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<Comment> findByNewsId(Long newId) throws ServiceException;

}
