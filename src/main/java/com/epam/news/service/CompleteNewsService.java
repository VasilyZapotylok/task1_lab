package com.epam.news.service;



import com.epam.news.entity.NewsDTO;
import com.epam.news.exception.ServiceException;

/**
 * The Interface CompleteNewsService.
 */
public interface CompleteNewsService {

	 /**
	 * Find complete news.
	 *
	 * @param newsId
	 *            the news id
	 * @return the news dto
	 * @throws ServiceException
	 *             the service exception
	 */
 	NewsDTO findCompleteNews(Long newsId) throws ServiceException;

	 /**
	 * Adds the complete news.
	 *
	 * @param newsDTO
	 *            the news dto
	 * @return the long
	 * @throws ServiceException
	 *             the service exception
	 */
 	Long addCompleteNews(NewsDTO newsDTO) throws ServiceException;

	 /**
	 * Dell complete news.
	 *
	 * @param newsId
	 *            the news id
	 * @return true, if successful
	 * @throws ServiceException
	 *             the service exception
	 */
 	boolean dellCompleteNews(Long newsId) throws ServiceException;

	 /**
	 * Update complete news.
	 *
	 * @param newsDTO
	 *            the news dto
	 * @return true, if successful
	 * @throws ServiceException
	 *             the service exception
	 */
 	boolean  updateCompleteNews(NewsDTO newsDTO) throws ServiceException;
}
