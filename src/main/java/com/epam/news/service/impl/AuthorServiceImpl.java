package com.epam.news.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.news.dao.impl.AuthorDAOImpl;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;

/**
 * The Class AuthorServiceImpl.
 */
@Service
public class AuthorServiceImpl implements AuthorService{

		/** The Constant LOGGER. */
		private static final Logger LOGGER = Logger.getLogger(AuthorServiceImpl.class);

	/** The author dao impl. */
	@Autowired
    private AuthorDAOImpl authorDAOImpl;

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#add(java.lang.Object)
	 */
	@Override
	public Long add(Author author) throws ServiceException {
		Long authortId;
		try {
			authortId =  authorDAOImpl.add(author);
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <author>", e);
		    throw new ServiceException(e);

		}
		return authortId;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#findById(java.lang.Long)
	 */
	@Override
	public Author findById(Long authorId) throws ServiceException {
		Author author;
		try {
			author =  authorDAOImpl.findById(authorId);
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <author>", e);
		    throw new ServiceException(e);

		}
		return author;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#update(java.lang.Object)
	 */
	@Override
	public boolean update(Author author) throws ServiceException {
		boolean flag;
		try {
			flag =  authorDAOImpl.update(author);
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <author>", e);
		    throw new ServiceException(e);

		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#deleteById(java.lang.Long)
	 */
	@Override
	public boolean deleteById(Long authorId) throws ServiceException {
		boolean flag;
		try {
			flag =  authorDAOImpl.deleteById(authorId);
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <author>", e);
		    throw new ServiceException(e);

		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#takeAll()
	 */
	@Override
	public List<Author> takeAll() throws ServiceException {
		List<Author> list = new ArrayList<Author>();
		try {
		    list = authorDAOImpl.findAll();
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <author>", e);
		    throw new ServiceException(e);

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.AuthorService#expireAuthor(com.epam.news.entity.Author)
	 */
	@Override
	public boolean expireAuthor(Author author) throws ServiceException {
		boolean flag;
		try {
			flag =  authorDAOImpl.expireAuthor(author);
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <author>", e);
		    throw new ServiceException(e);

		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.AuthorService#findByNewsId(java.lang.Long)
	 */
	@Override
	public List<Author> findByNewsId(Long newsId) throws ServiceException {
		List<Author> list = new ArrayList<Author>();
		try {
		    list = authorDAOImpl.findByNewsId(newsId);
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <author>", e);
		    throw new ServiceException(e);

		}
		return list;
	}

}
