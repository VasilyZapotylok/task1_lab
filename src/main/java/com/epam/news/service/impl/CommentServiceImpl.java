package com.epam.news.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.news.dao.impl.CommentDAOImpl;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.CommentService;

/**
 * The Class CommentServiceImpl.
 */
@Service
public class CommentServiceImpl implements CommentService{

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(CommentServiceImpl.class);

	/** The comment dao impl. */
	@Autowired
    private CommentDAOImpl commentDAOImpl;

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#add(java.lang.Object)
	 */
	@Override
	public Long add(Comment comment) throws ServiceException {
		Long commentId;
		try {
			commentId =  commentDAOImpl.add(comment);
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <comment>", e);
		    throw new ServiceException(e);

		}
		return commentId;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#findById(java.lang.Long)
	 */
	@Override
	public Comment findById(Long commentId) throws ServiceException {
		Comment comment;
		try {
			comment =  commentDAOImpl.findById(commentId);
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <comment>", e);
		    throw new ServiceException(e);

		}
		return comment;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#update(java.lang.Object)
	 */
	@Override
	public boolean update(Comment comment) throws ServiceException {
		boolean flag;
		try {
			flag =  commentDAOImpl.update(comment);
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <comment>", e);
		    throw new ServiceException(e);

		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#deleteById(java.lang.Long)
	 */
	@Override
	public boolean deleteById(Long commentId) throws ServiceException {
		boolean flag;
		try {
			flag =  commentDAOImpl.deleteById(commentId);
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <comment>", e);
		    throw new ServiceException(e);

		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#takeAll()
	 */
	@Override
	public List<Comment> takeAll() throws ServiceException {
		List<Comment> list = new ArrayList<Comment>();
		try {
		    list = commentDAOImpl.findAll();
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <comment>", e);
		    throw new ServiceException(e);

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.CommentService#findByNewId(java.lang.Long)
	 */
	@Override
	public List<Comment> findByNewsId(Long newsId) throws ServiceException {
		List<Comment> list = new ArrayList<Comment>();
		try {
		    list = commentDAOImpl.findByNewsId(newsId);
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <comment>", e);
		    throw new ServiceException(e);

		}
		return list;
	}


}
