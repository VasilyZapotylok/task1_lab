package com.epam.news.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.entity.Author;
import com.epam.news.entity.NewsDTO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.CompleteNewsService;

/**
 * The Class CompleteNewsServiceImpl.
 */
@Service
public class CompleteNewsServiceImpl implements CompleteNewsService {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(CompleteNewsServiceImpl.class);

	/** The news service impl. */
	@Autowired
	private NewsServiceImpl newsServiceImpl;

	/** The author service impl. */
	@Autowired
	private AuthorServiceImpl authorServiceImpl;

	/** The tag service impl. */
	@Autowired
	private TagServiceImpl tagServiceImpl;

	/** The Comment service impl. */
	@Autowired
	private CommentServiceImpl CommentServiceImpl;

	/* (non-Javadoc)
	 * @see com.epam.news.service.CompleteNewsService#findCompleteNews(java.lang.Long)
	 */
	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public NewsDTO findCompleteNews(Long newsId) throws ServiceException {
		NewsDTO newsDTO = new NewsDTO();
		try {
			newsDTO.setNews(newsServiceImpl.findById(newsId));
			newsDTO.setAuthors(authorServiceImpl.findByNewsId(newsId));
			newsDTO.setTags(tagServiceImpl.findByNewsId(newsId));
			newsDTO.setComments(CommentServiceImpl.findByNewsId(newsId));
		} catch (ServiceException e) {
			LOGGER.error("Problem caused the database access", e);
			throw new ServiceException(e);
		}
		return newsDTO;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.CompleteNewsService#addCompleteNews(com.epam.news.entity.NewsDTO)
	 */
	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public Long addCompleteNews(NewsDTO newsDTO) throws ServiceException {
		Long newId;
		try {
			newId = newsServiceImpl.add(newsDTO.getNews());
			newsServiceImpl.addNewsAuthorList(newId, getAuthorsIds(newsDTO));
			newsServiceImpl.addNewsTagList(newId, getTagsIds(newsDTO));
		} catch (ServiceException e) {
			LOGGER.error("Problem caused the database access", e);
			throw new ServiceException(e);
		}
		return newId;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.CompleteNewsService#dellCompleteNews(java.lang.Long)
	 */
	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public boolean dellCompleteNews(Long newsId) throws ServiceException {
		boolean flag = false;
		try {
			flag = newsServiceImpl.delAllCommentByNewsId(newsId);
			flag = newsServiceImpl.delNewsAuthorList(newsId);
			flag = newsServiceImpl.delNewsTagList(newsId);
			flag = newsServiceImpl.deleteById(newsId);
		} catch (ServiceException e) {
			LOGGER.error("Problem caused the database access", e);
			throw new ServiceException(e);
		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.CompleteNewsService#updateCompleteNews(com.epam.news.entity.NewsDTO)
	 */
	@Transactional(rollbackFor = ServiceException.class)
	@Override
	public boolean updateCompleteNews(NewsDTO newsDTO) throws ServiceException {
		boolean flag = false;
		try {
			flag = newsServiceImpl.update(newsDTO.getNews());
			flag = newsServiceImpl.delNewsAuthorList(newsDTO.getNews().getNewsId());
			flag = newsServiceImpl.addNewsAuthorList(newsDTO.getNews().getNewsId(), getAuthorsIds(newsDTO));
			flag = newsServiceImpl.delNewsTagList(newsDTO.getNews().getNewsId());
			flag = newsServiceImpl.addNewsTagList(newsDTO.getNews().getNewsId(), getTagsIds(newsDTO));
		} catch (ServiceException e) {
			LOGGER.error("Problem caused the database access", e);
			throw new ServiceException(e);
		}
		return flag;
	}

	/**
	 * Gets the tags ids.
	 *
	 * @param newsDTO
	 *            the news dto
	 * @return the tags ids
	 */
	private List<Long> getTagsIds(NewsDTO newsDTO) {
		List<Tag> tagList = newsDTO.getTags();
		List<Long> tagsIdList = new ArrayList<Long>();
		for (int i = 0; i < tagList.size(); i++) {
			tagsIdList.add(tagList.get(i).getTagId());
		}
		return tagsIdList;
	}

	/**
	 * Gets the authors ids.
	 *
	 * @param newsDTO
	 *            the news dto
	 * @return the authors ids
	 */
	private List<Long> getAuthorsIds(NewsDTO newsDTO) {
		List<Author> authorList = newsDTO.getAuthors();
		List<Long> authorsIdList = new ArrayList<Long>();
		for (int i = 0; i < authorList.size(); i++) {
			authorsIdList.add(authorList.get(i).getAuthorId());
		}
		return authorsIdList;
	}

}
