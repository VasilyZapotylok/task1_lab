package com.epam.news.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.news.dao.impl.NewsDAOImpl;
import com.epam.news.entity.News;
import com.epam.news.entity.SearchCase;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.NewsService;

/**
 * The Class NewsServiceImpl.
 */
@Service
public class NewsServiceImpl implements NewsService {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(NewsServiceImpl.class);

	/** The news dao impl. */
	@Autowired
	private NewsDAOImpl newsDAOImpl;

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#add(java.lang.Object)
	 */
	@Override
	public Long add(News news) throws ServiceException {
		Long newsId;
		try {
			newsId = newsDAOImpl.add(news);
		} catch (DAOException e) {
			LOGGER.error("Problem caused the database access <new>", e);
			throw new ServiceException(e);

		}
		return newsId;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#findById(java.lang.Long)
	 */
	@Override
	public News findById(Long newsId) throws ServiceException {
		News news;
		try {
			news = newsDAOImpl.findById(newsId);
		} catch (DAOException e) {
			LOGGER.error("Problem caused the database access <new>", e);
			throw new ServiceException(e);

		}
		return news;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#update(java.lang.Object)
	 */
	@Override
	public boolean update(News news) throws ServiceException {
		boolean flag;
		try {
			flag = newsDAOImpl.update(news);
		} catch (DAOException e) {
			LOGGER.error("Problem caused the database access <new>", e);
			throw new ServiceException(e);

		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#deleteById(java.lang.Long)
	 */
	@Override
	public boolean deleteById(Long newsId) throws ServiceException {
		boolean flag;
		try {
			flag = newsDAOImpl.deleteById(newsId);
		} catch (DAOException e) {
			LOGGER.error("Problem caused the database access <new>", e);
			throw new ServiceException(e);

		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#takeAll()
	 */
	@Override
	public List<News> takeAll() throws ServiceException {
		List<News> list = new ArrayList<News>();
		try {
			list = newsDAOImpl.findAll();
		} catch (DAOException e) {
			LOGGER.error("Problem caused the database access <new>", e);
			throw new ServiceException(e);

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsService#findByAuhtorId(java.lang.Long)
	 */
	@Override
	public List<News> findByAuhtorId(Long authorId) throws ServiceException {
		List<News> list = new ArrayList<News>();
		try {
			list = newsDAOImpl.findByAuhtorId(authorId);
		} catch (DAOException e) {
			LOGGER.error("Problem caused the database access <new>", e);
			throw new ServiceException(e);

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsService#findByTagId(java.lang.Long)
	 */
	@Override
	public List<News> findByTagId(Long tagId) throws ServiceException {
		List<News> list = new ArrayList<News>();
		try {
			list = newsDAOImpl.findByTagId(tagId);
		} catch (DAOException e) {
			LOGGER.error("Problem caused the database access <new>", e);
			throw new ServiceException(e);

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsService#findNewsBySearchCase(com.epam.news.entity.SearchCase)
	 */
	@Override
	public List<News> findNewsBySearchCase(SearchCase searchCase) throws ServiceException {
		List<News> list = new ArrayList<News>();
		try {
			list = newsDAOImpl.findNewsBySearchCase(searchCase);
		} catch (DAOException e) {
			LOGGER.error("Problem caused the database access <new>", e);
			throw new ServiceException(e);

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsService#addNewsAuthorList(java.lang.Long, java.util.List)
	 */
	@Override
	public boolean addNewsAuthorList(Long newsId, List<Long> authorsId) throws ServiceException {
		boolean flag;
		try {
			flag = newsDAOImpl.addNewsAuthorList(newsId, authorsId);
		} catch (DAOException e) {
			LOGGER.error("Problem caused the database access <new>", e);
			throw new ServiceException(e);

		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsService#addNewsTagList(java.lang.Long, java.util.List)
	 */
	@Override
	public boolean addNewsTagList(Long newsId, List<Long> tagsId) throws ServiceException {
		boolean flag;
		try {
			flag = newsDAOImpl.addNewsTagList(newsId, tagsId);
		} catch (DAOException e) {
			LOGGER.error("Problem caused the database access <new>", e);
			throw new ServiceException(e);

		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsService#countAllNews()
	 */
	@Override
	public Long countAllNews() throws ServiceException {
		Long count;
		try {
			count = newsDAOImpl.countAllNews();
		} catch (DAOException e) {
			LOGGER.error("Problem caused the database access <new>", e);
			throw new ServiceException(e);

		}
		return count;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsService#findLastNewsByAmount(java.lang.Long)
	 */
	@Override
	public List<News> findLastNewsByAmount(Long amount) throws ServiceException {
		List<News> list = new ArrayList<News>();
		try {
			list = newsDAOImpl.findLastNewsByAmount(amount);
		} catch (DAOException e) {
			LOGGER.error("Problem caused the database access <new>", e);
			throw new ServiceException(e);

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsService#delAllCommentByNewsId(java.lang.Long)
	 */
	@Override
	public boolean delAllCommentByNewsId(Long newsId) throws ServiceException {
		boolean flag;
		try {
			flag = newsDAOImpl.delAllCommentByNewsId(newsId);
		} catch (DAOException e) {
			LOGGER.error("Problem caused the database access <comments>", e);
			throw new ServiceException(e);

		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsService#delNewsAuthorList(java.lang.Long)
	 */
	@Override
	public boolean delNewsAuthorList(Long newsId) throws ServiceException {
		boolean flag;
		try {
			flag = newsDAOImpl.delNewsAuthorList(newsId);
		} catch (DAOException e) {
			LOGGER.error("Problem caused the database access <news_author>", e);
			throw new ServiceException(e);

		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsService#delNewsTagList(java.lang.Long)
	 */
	@Override
	public boolean delNewsTagList(Long newsId) throws ServiceException {
		boolean flag;
		try {
			flag = newsDAOImpl.delNewsTagList(newsId);
		} catch (DAOException e) {
			LOGGER.error("Problem caused the database access <news_tag>", e);
			throw new ServiceException(e);

		}
		return flag;
	}

}
