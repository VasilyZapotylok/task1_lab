package com.epam.news.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.epam.news.dao.impl.TagDAOImpl;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.TagService;

/**
 * The Class TagServiceImpl.
 */
@Service
public class TagServiceImpl implements TagService {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(TagServiceImpl.class);

	/** The tag dao impl. */
	@Autowired
    private TagDAOImpl tagDAOImpl;

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#add(java.lang.Object)
	 */
	@Override
	public Long add(Tag tag) throws ServiceException {
		Long tagId;
		try {
		    tagId = tagDAOImpl.add(tag);
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <tag>", e);
		    throw new ServiceException(e);

		}
		return tagId;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#findById(java.lang.Long)
	 */
	@Override
	public Tag findById(Long tagId) throws ServiceException {
		Tag tag;
		try {
		    tag = tagDAOImpl.findById(tagId);
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <tag>", e);
		    throw new ServiceException(e);

		}
		return tag;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#update(java.lang.Object)
	 */
	@Override
	public boolean update(Tag tag) throws ServiceException {
		boolean flag;
		try {
		    flag = tagDAOImpl.update(tag);
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <tag>", e);
		    throw new ServiceException(e);

		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#deleteById(java.lang.Long)
	 */
	@Override
	public boolean deleteById(Long tagId) throws ServiceException {
		boolean flag;
		try {
		    flag = tagDAOImpl.deleteById(tagId);
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <tag>", e);
		    throw new ServiceException(e);

		}
		return flag;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.Service#takeAll()
	 */
	@Override
	public List<Tag> takeAll() throws ServiceException {
		List<Tag> list = new ArrayList<Tag>();
		try {
		    list = tagDAOImpl.findAll();
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <tag>", e);
		    throw new ServiceException(e);

		}
		return list;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.TagService#findByNewsId(java.lang.Long)
	 */
	@Override
	public List<Tag> findByNewsId(Long newsId) throws ServiceException {
		List<Tag> list = new ArrayList<Tag>();
		try {
		    list = tagDAOImpl.findByNewsId(newsId);
		} catch (DAOException e) {
		    LOGGER.error("Problem caused the database access <tag>", e);
		    throw new ServiceException(e);

		}
		return list;
	}

}


