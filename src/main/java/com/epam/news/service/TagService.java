package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;

/**
 * The Interface TagService.
 */
public interface TagService extends Service<Tag> {

	/**
	 * Find by news id.
	 *
	 * @param newsId
	 *            the news id
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<Tag> findByNewsId(Long newsId) throws ServiceException;

}
