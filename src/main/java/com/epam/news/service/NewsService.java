package com.epam.news.service;


import java.util.List;

import com.epam.news.entity.News;
import com.epam.news.entity.SearchCase;
import com.epam.news.exception.ServiceException;

/**
 * The Interface NewsService.
 */
public interface NewsService extends Service<News> {

    /**
	 * Find by auhtor id.
	 *
	 * @param authorId
	 *            the author id
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
    List<News> findByAuhtorId(Long authorId) throws ServiceException;

    /**
	 * Find by tag id.
	 *
	 * @param tagId
	 *            the tag id
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
    List<News> findByTagId(Long tagId) throws ServiceException;

    /**
	 * Find news by search case.
	 *
	 * @param searchCase
	 *            the search case
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
    List<News> findNewsBySearchCase(SearchCase searchCase) throws ServiceException;

    /**
	 * Adds the news author list.
	 *
	 * @param newsId
	 *            the news id
	 * @param authorsId
	 *            the authors id
	 * @return true, if successful
	 * @throws ServiceException
	 *             the service exception
	 */
    boolean addNewsAuthorList(Long newsId, List<Long> authorsId) throws ServiceException;

    /**
	 * Adds the news tag list.
	 *
	 * @param newsId
	 *            the news id
	 * @param tagsId
	 *            the tags id
	 * @return true, if successful
	 * @throws ServiceException
	 *             the service exception
	 */
    boolean addNewsTagList(Long newsId, List<Long> tagsId) throws ServiceException;

    /**
	 * Del all comment by news id.
	 *
	 * @param newsId
	 *            the news id
	 * @return true, if successful
	 * @throws ServiceException
	 *             the service exception
	 */
    boolean delAllCommentByNewsId(Long newsId) throws ServiceException;

    /**
	 * Del news author list.
	 *
	 * @param newsId
	 *            the news id
	 * @return true, if successful
	 * @throws ServiceException
	 *             the service exception
	 */
    boolean delNewsAuthorList(Long newsId) throws ServiceException;

    /**
	 * Del news tag list.
	 *
	 * @param newsId
	 *            the news id
	 * @return true, if successful
	 * @throws ServiceException
	 *             the service exception
	 */
    boolean delNewsTagList(Long newsId) throws ServiceException;

    /**
	 * Count all news.
	 *
	 * @return the long
	 * @throws ServiceException
	 *             the service exception
	 */
    Long countAllNews() throws ServiceException;

    /**
	 * Find last news by amount.
	 *
	 * @param amount
	 *            the amount
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
    List<News> findLastNewsByAmount(Long amount) throws ServiceException;


}
