package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.exception.ServiceException;


/**
 * The Interface AuthorService.
 */
public interface AuthorService extends Service<Author>{

	/**
	 * Expire author.
	 *
	 * @param author
	 *            the author
	 * @return true, if successful
	 * @throws ServiceException
	 *             the service exception
	 */
	boolean expireAuthor(Author author) throws ServiceException;

	/**
	 * Find by news id.
	 *
	 * @param newsId
	 *            the news id
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<Author> findByNewsId(Long newsId) throws ServiceException;

}
